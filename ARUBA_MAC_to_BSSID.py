#This is free and unencumbered software released into the public domain.

#Anyone is free to copy, modify, publish, use, compile, sell, or
#distribute this software, either in source code form or as a compiled
#binary, for any purpose, commercial or non-commercial, and by any
#means.

#In jurisdictions that recognize copyright laws, the author or authors
#of this software dedicate any and all copyright interest in the
#software to the public domain. We make this dedication for the benefit
#of the public at large and to the detriment of our heirs and
#successors. We intend this dedication to be an overt act of
#relinquishment in perpetuity of all present and future rights to this
#software under copyright law.

#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
#EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
#MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
#IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
#OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
#ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
#OTHER DEALINGS IN THE SOFTWARE.

#For more information, please refer to <http://unlicense.org/>

#Aruba BSSID from Ethernet MAC
#Author - Kieran Morton - mortonese.com

#User Inputs
input_mac = input("Enter Wired MAC Address: ") #User Inputs Wired MAC Address
input_bssid = input ("Enter Number of BSSID's per Radio: ") #User Input Number of BSSID's per Radio

clean_mac = input_mac.replace(":", "") #Remove Colons from inputted Wired MAC Address

nic = clean_mac[6:16] #Extract NIC from total Wired MAC Address

nic = nic[1:16] #Remove first character from the NIC

binary_nic = format(int(nic, 16), "020b") #Convert to binary string

binary_nic = binary_nic + "0000" #Append 0000 to binary string

#XOR first four digits with 8 (in binary)
a = binary_nic[0:4] #Get first 4 binary digits of the NIC
b = "1000" #Decimal 8 in binary
y = int(a, 2)^int(b,2) # XOR with 8 or 1000 in binary
z = bin(y)[2:].zfill(len(a)) #Remove '0b' and pad to make length 4
binary_r0_nic = z + binary_nic[4:] #Replace first four digits with the XOR result to derive Radio 0 NIC

#Get other base MAC
binary_bssid = format(int(input_bssid, 10), "020b").zfill(len(binary_r0_nic)) #Convert number of BSSIDs to binary number
binary_r1_nic = bin(int(binary_r0_nic, 2) + int(binary_bssid, 2))[2:] #Add Binary NO. BSSIDs to the Binary NIC to derive Radio 1 NIC

# Convert binary NICs back to HEX (and remove '0x')
r0_nic = hex(int(binary_r0_nic, 2))[2:]
r1_nic = hex(int(binary_r1_nic, 2))[2:]

#Add NICs back to original OUI
r0_mac = clean_mac[0:6] + r0_nic
r1_mac = clean_mac[0:6] + r1_nic

#Add the colons back
r0_mac = (':'.join(r0_mac[i:i+2] for i in range(0, len(r0_mac), 2))).upper()
r1_mac = (':'.join(r1_mac[i:i+2] for i in range(0, len(r1_mac), 2))).upper()

#Print results
print("Radio 0 Base MAC Address: " + r0_mac)
print("Radio 1 Base MAC Address: " + r1_mac)
